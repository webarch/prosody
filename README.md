Ansible Playbook to install and configure a [Prosody](https://prosody.im/) XMPP chat server and Apache on a Debian Stretch virtual server, this has been written with the intention of intergrating it into the [Nextcloud Playbook](https://git.coop/webarch/nextcloud).

First run a suitable Sudoers Playbook and then the [Webarchitects Ansible Playbook](https://git.coop/webarch/ansible#webarchitects-co-operative-ansible-playbooks-for-debian-servers).

Setup the DNS, for example:

```bind
; prosody test server
prosody                                 IN      A       81.95.52.58
www.prosody                             IN      A       81.95.52.58
xmpp.prosody                            IN      A       81.95.52.58
userdata.prosody                        IN      A       81.95.52.58
pubsub.prosody                          IN      A       81.95.52.58
conference.prosody                      IN      A       81.95.52.58
_xmpp-client._tcp.prosody               SRV     10 1 5222 xmpp.prosody.webarchitects.org.uk.
_xmpp-server._tcp.prosody               SRV     10 1 5269 xmpp.prosody.webarchitects.org.uk.
_xmpp-server._tcp.conference.prosody    SRV     10 1 5269 xmpp.prosody.webarchitects.org.uk.
_xmpp-server._tcp.pubsub.prosody        SRV     10 1 5269 xmpp.prosody.webarchitects.org.uk.
```

And then run:


```bash
export SERVERNAME="prosody.webarchitects.org.uk"
ansible-playbook prosody.yml  -i ${SERVERNAME}, -e "hostname=${SERVERNAME}"
```

The `admin@${SERVERNAME}` XMPP account password is written to `/root/.prosody` and additional accounts can be created using [prosodyctl](https://prosody.im/doc/prosodyctl), for example:

```bash
sudo -i
prosodyctl adduser chris@$( hostname -f )

```

Test the results at https://check.messaging.one/
